import React, {useState,useEffect} from 'react';
import axios from 'axios';
import moment from 'moment';
import {useForm} from 'react-hook-form'
import App from '../App'

const Update = props => {

	const url = "http://127.0.0.1:8080/api/books/" + props.match.params.id;
	const Role = sessionStorage.getItem('roles');

	const [form,setForm] = useState({
		title:"",
		author:"",
		pages:"",
		language:"",
	})

	useEffect(() =>{
		axios.get(url,{headers: {
			"Authorization" : sessionStorage.getItem('token')
		  }
		})
		.then(res=>{
			console.table(res.data.book)
			setForm(res.data)

		}).catch(err=>console.error(err))
	},[])

	const Submit = () => {
		axios.put(url,form,{headers: {
			"Authorization" : sessionStorage.getItem('token')
		  }
		})
		.then(res=>{
			console.log(res.data)
			props.history.push("/book")
		}).catch(err=>console.error(err))
	}

	const handleChange = (e) => {
		const {name,value} = e.target
		setForm({
			...form,
			[name] : value
		})
	}

	const { register, handleSubmit, errors } = useForm({
		mode: "onChange"
	});

	return (
		<>
		{Role === '2' ?
		<div className="container col-10">
			<div className="row mt-5">
			<div className="col-7">
			<div className="card w-responsive">
				<div className="card-header">
					Edit Buku
				</div>
				<div className="card-body w-responsive">
			<form onSubmit={(handleSubmit(Submit))}>
			<div className="container">
			<div className="form-group">
				<label>Author</label>
				<input
					onChange={(e)=>handleChange(e)}
					value={form.title}
					type="text"
					id="title"
					name="title"
					className="form-control"
					placeholder="Masukan Author"
					ref={register({
						required: "This is required",
						minLength: {
							value: 3,
							message: "Min length is 3"
						}
					})}
				/>
				{errors.author && <p className="text-danger">{errors.author.message}</p>}
			</div>
				<div className="form-group">
					<label>Author</label>
					<input
						onChange={(e)=>handleChange(e)}
						value={form.author}
						type="text"
						id="author"
						name="author"
						className="form-control"
						placeholder="Masukan Author"
						ref={register({
							required: "This is required",
							minLength: {
								value: 3,
								message: "Min length is 3"
							}
						})}
					/>
					{errors.author && <p className="text-danger">{errors.author.message}</p>}
				</div>
				<div className="form-group">
					<label>Pages</label>
					<input
						onChange={(e)=>handleChange(e)}
						value={form.pages}
						type="number"
						id="pages"
						name="pages"
						className="form-control"
						placeholder="Masukan Pages"
					/>
					{errors.pages && <p style={{color: "red"}}>{errors.pages}</p> }
				</div>
				<div className="form-group">
					<label>Language</label>
					<input
						onChange={(e)=>handleChange(e)}
						value={form.language}
						type="text"
						id="language"
						name="language"
						className="form-control"
						placeholder="Masukan Language"
						ref={register({
							required: "This is required"
						})}
					/>
					{errors.language && <p className="text-danger">{errors.language.message}</p>}
				</div>
					<button className="btn btn-primary">Submit</button>
				</div>
			</form>
            </div>
			</div>
			</div>
			</div>
			</div>:
				<App />
			}
			</>
	)
}

export default Update
