import React, {useState,useMemo} from "react";
import axios from "axios";
import moment from "moment";
import "../App.css";
import logo from '../logo.svg';
import Comment from '../Comment/Comment';

const Detail = (props) => {

  const url = "http://127.0.0.1:8080/api/books/" + props.match.params.id;

  const [books,setBooks] = useState({
    book:{
      title:"",
      author:"",
      pages:"",
      language:"",
        comments:[
          {
          comment:"",
          user:{
            username:""
          }
        }
    ]
  }
  })

  useMemo(()=>{
    axios.get(url,{headers: {
    "Authorization" : sessionStorage.getItem('token')
    }
})
  .then(res=>{
     console.log(res.data)
    setBooks(res.data)
  })
  },[])

    return (
      <>
        <div className="row container">
        <h1 className="col-5 mt-5">{books.title}</h1>
        <h5 className="col-8 ">Author : {books.author} </h5>
        <h5 className="col-5 ">Language : {books.language}</h5>
        <div class="post-meta">

          <span class="mr-4">{books.createdAt} </span>
          <span class="ml-2"><span class="fa fa-comments"></span> 3</span>
        </div>
        <div className="post-content-body col-9 mt-2">

          <p className="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium nam quas inventore, ut iure iste modi eos adipisci ad ea itaque labore earum autem nobis et numquam, minima eius. Nam eius, non unde ut aut sunt eveniet rerum repellendus porro.</p>
        <p>Sint ab voluptates itaque, ipsum porro qui obcaecati cumque quas sit vel. Voluptatum provident id quis quo. Eveniet maiores perferendis officia veniam est laborum, expedita fuga doloribus natus repellendus dolorem ab similique sint eius cupiditate necessitatibus, magni nesciunt ex eos.</p>
        <p>Quis eius aspernatur, eaque culpa cumque reiciendis, nobis at earum assumenda similique ut? Aperiam vel aut, ex exercitationem eos consequuntur eaque culpa totam, deserunt, aspernatur quae eveniet hic provident ullam tempora error repudiandae sapiente illum rerum itaque voluptatem. Commodi, sequi.</p>

        </div>
            {books.comments ?
              <>
            {books.comments.map((value,index)=>(
              <ul className="comment-list col-15 container">
                <li className="comment">
                  <div className="comment-body">
                    <h3>{value.user.username}</h3>
                    <div className="meta">{moment(value.createdAt).format("DD-MM-YYYY")}</div>
                    <div className="meta">{moment(value.createdAt).format("LT")}</div>
                    <p>{value.comment}</p>

                  </div>
                </li>

              </ul>
            )) }
              </>
              : <><h1>WKWKWKWK</h1></>
            }
            <Comment />
        </div>
      </>
    )
}

export default Detail
