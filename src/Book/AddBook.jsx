import React, {useState,useEffect} from 'react';
import axios from 'axios';
import momet from 'moment';

import {useForm} from 'react-hook-form'


const Create = (props) => {
	const url = "http://127.0.0.1:8080/api/books/"

	const [form,setForm] = useState({
		title:"",
		author:"",
		published_date:"",
		pages:"",
		language:"",
		publisher_id:""
	})




	const Submit = (e) => {
		axios.post(url,form,{headers: {
			"Authorization" : sessionStorage.getItem('token')
		  }
		})
		.then(res=>{
			console.log(res.data)
			const myData =({...form})
			setForm(myData)
			window.location="/book"
		}).catch(err=>console.error(err))
	}

	const handleChange = (e) => {
		const {name,value} = e.target
		setForm({
			...form,
			[name] : value
		})
	}
		// const newData={...form}
		// newData[e.target.id] = e.target.value
		// setForm(newData)


	const { register, handleSubmit, errors } = useForm({
		mode: "onChange"
	});

	return (
		<div className="container">
			<div className="card w-responsive">
				<div className="card-header">
					Featured
				</div>
				<div className="card-body w-responsive">
			<form onSubmit={handleSubmit(Submit)}>

				<div className="form-group">
					<label>Title</label>
					<input
						onChange={(e)=>handleChange(e)}
						value={form.title}
						type="text"
						id="title"
						name="title"
						className="form-control"
						placeholder="Masukan Title"
						ref={register({
							required: "This is required",
							minLength: {
								value: 3,
								message: "Min length is 3"
							}
						})}
					/>
					{errors.title && <p className="text-danger">{errors.title.message}</p>}
				</div>

				<div className="form-group">
					<label>Author</label>
					<input
						onChange={(e)=>handleChange(e)}
						value={form.author}
						type="text"
						id="author"
						name="author"
						className="form-control"
						placeholder="Masukan Author"
						ref={register({
							required: "This is required",
							minLength: {
								value: 3,
								message: "Min length is 3"
							}
						})}
					/>
					{errors.author && <p className="text-danger">{errors.author.message}</p>}
				</div>

				<div className="form-group">
					<label htmlFor="pages">Pages</label>
					<input
						id="pages"
						type="number"
						placeholder="Pages"
						name="pages"
						value={form.pages}
						onChange={(e)=>handleChange(e)}
						className="form-control"
						ref={register({
							required: "This is required",
						})}
					/>
					{errors.pages && <p className="text-danger">{errors.pages.message}</p>}
				</div>
				<div className="form-group">
					<label>Language</label>
					<input
						onChange={(e)=>handleChange(e)}
						value={form.language}
						type="text"
						id="language"
						name="language"
						className="form-control"
						placeholder="Masukan Language"
						ref={register({
							required: "This is required"
						})}
					/>
					{errors.language && <p className="text-danger">{errors.language.message}</p>}
				</div>
				<button className="btn btn-primary">Submit</button>

			</form>
			</div>
			</div>
		</div>
	)
}

export default Create
