import React, { useState,  useEffect } from 'react'
import axios from 'axios'
import Create from './AddBook'
import App from '../App'

const BookList = (props) => {

	const url = "http://127.0.0.1:8080/api/books/"
	const  [book,setBook] = useState({book:[]})
	const Role = sessionStorage.getItem('roles');

	useEffect(() => {
		const fetchData = async () => {
		  const result = await axios(url,{headers: {
			"Authorization" : sessionStorage.getItem('token')
		  }
		});
		console.table(result.data.book)
		  setBook(result.data);
		};
		fetchData();
	  }, []);

	const Update = (id) => {
		console.log(id)
		props.history.push("/update/"+id)
	}

	const Remove = (id) => {
		console.log(id)
		axios.delete(url+id,{headers: {
			"Authorization" : sessionStorage.getItem('token')
		  }
		})
		.then(res=>{
			window.location.replace('/book')
		}).catch(err=>console.error(err))
	}

	const Detail = (id) => {
		props.history.push('/detail/' + id)
	}


	const display = book.book.map(books=>
		<tr key={books.id}>
			<td>{books.title}</td>
			<td>{books.author}</td>
			<td>{books.pages}</td>
			<td>{books.language}</td>
			<td><button onClick={()=>Update(books.id)} className="btn btn-outline-success">Update</button></td>
			<td><button onClick={()=>Remove(books.id)} className="btn btn-outline-danger">Delete</button></td>
			
		</tr>
	)

	return (
				<>
				{Role === '2' ?
				<div className="row mt-5">
					{/* <button onClick={()=>Tambah()} className="btn btn-primary">Tambah</button>
					<br /> */}
					<div className="col-5 w-responsive">
					<Create />
					</div>

					<div className="col-15 w-responsive">
					<table className="table table-responsive">
						<thead>
							<tr>
								<th>Title</th>
								<th>Author</th>
								<th>Pages</th>
								<th>Language</th>
								<th colSpan="3">Action</th>
							</tr>
						</thead>
						<tbody>
							 {display}
						</tbody>
					</table>
					</div>

				</div>:
					<App />
				}
				</>
	)
}

export default BookList
