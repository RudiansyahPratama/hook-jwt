import React, { useContext } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Nav from './Navs/Nav';
import BookList from './Book/BookList';
import Create from './Book/AddBook';
import Update from './Book/UpdateBook';
import GlobalHook from './GlobalHook/GlobalHook';
import Login from './Auth/Login';
import Register from './Auth/Register';
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import UsersList from './User/UserList';
import Detail from './Book/Detail';
import UserProfile from './User/UserProfile';



const routing = (
    <Router>
        <Nav />
        <Switch>
            <Route exact path="/" component={App} />
            <Route path="/book" component={BookList} />
            <Route path="/user" component={UsersList} />
            <Route path="/addBook" component={Create} />
            <Route path="/update/:id" component={Update} />
            <Route path="/login/" component={Login} />
            <Route path="/detail/:id" component={Detail}/>
            <Route path="/register/" component={Register} />
            <Route path="/globalhook" component={GlobalHook} />
            <Route path="/profile" component={UserProfile} />
        </Switch>
    </Router>
);
ReactDOM.render(routing, document.getElementById("root"));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
