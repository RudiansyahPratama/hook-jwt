import React ,{useState} from 'react';
import axios from 'axios';
import logo from '../logo.svg';

 const SignIn = (props) => {

    const url = "http://127.0.0.1:8080/api/auth/signin"

    const [form,setForm ] = useState({
        username:"",
        password:""
    })

    const handleSubmit = (e) => {
        e.preventDefault()
        axios.post(url,form)
        .then(res=>{
            if(res.status === 200){
                sessionStorage.setItem("token",res.data.accessToken);
                sessionStorage.setItem("roles",res.data.roles);
                alert(
                    `Login Succes \n Your Acces Token is : ${sessionStorage.getItem('token')}`
                )
                window.location.replace("/book")
            }
        })
        .catch(function(err){
            console.log(err)
            alert('Gagal Login')

        })
    }

    const handleChange = (e) => {
		const {name,value} = e.target
		setForm({
			...form,
			[name] : value
		})
	}

    return (
      <div className="row mt-5">
        <div className="container col-5">
          <div className="card text-center">
            <div className="card-header">
              SignIn
            </div>
            <div className="card-body">
                <form onSubmit={(e)=>handleSubmit(e)}>
                <div className="mb-2">
                  <label htmlFor="validationServer02">Username</label>
                  <input type="text"
                    className="form-control is-valid"
                    id="username"
                    onChange={(e)=>handleChange(e)}
                    name="username"
                    required
                  />
                  <div className="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <div className="mb-3">
                  <label htmlFor="validationServer02">Password</label>
                  <input
                    className="form-control "
                    id="password"
                    type="password"
                    name="password"
                    onChange={(e)=>handleChange(e)}
                    required
                  />
                  <div className="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <button className="btn btn-outline-primary">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>
    )

}

export default SignIn
