import React ,{useState} from 'react';
import axios from 'axios';
import logo from '../logo.svg';

 const SignIn = (props) => {

    const url = "http://127.0.0.1:8080/api/auth/signup"

    const [form,setForm ] = useState({
        name:"",
        username:"",
        email:"",
        password:"",
        roles:["USER"]
    })

    const handleSubmit = (e) => {
        e.preventDefault()
        axios.post(url,form)
        .then(res=>{
           console.log(res.data)
           props.history.push("/login")
           
        })
        .catch(function(err){
            console.log(err)
        })
    }

    const handleChange = (e) => {
		const {name,value} = e.target
		setForm({
			...form,
			[name] : value
		})
	}

    return (
      <div className="row mt-5">
        <div className="container col-5">
          <div className="card text-center">
            <div className="card-header">
              SignIn
            </div>
            <div className="card-body">
                <form onSubmit={(e)=>handleSubmit(e)}>
                <div className="mb-2">
                  <label htmlFor="validationServer02">Name</label>
                  <input type="text" 
                    className="form-control is-valid"
                    id="name" 
                    onChange={(e)=>handleChange(e)} 
                    name="name" 
                    required 
                  />
                  <div className="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <div className="mb-2">
                  <label htmlFor="validationServer02">Username</label>
                  <input type="text" 
                    className="form-control is-valid"
                    id="username" 
                    onChange={(e)=>handleChange(e)} 
                    name="username" 
                    required 
                  />
                  <div className="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <div className="mb-2">
                  <label htmlFor="validationServer02">Email</label>
                  <input type="text" 
                    className="form-control is-valid"
                    id="email" 
                    onChange={(e)=>handleChange(e)} 
                    name="email" 
                    required 
                  />
                  <div className="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <div className="mb-3">
                  <label htmlFor="validationServer02">Password</label>
                  <input 
                    className="form-control " 
                    id="password" 
                    type="password"
                    name="password"
                    onChange={(e)=>handleChange(e)}
                    required 
                  />
                  <div className="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <button className="btn btn-outline-primary">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>
    )

}

export default SignIn