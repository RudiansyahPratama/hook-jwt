import React, { useEffect,useState } from 'react';
import axios from 'axios';

const UserProfile = (props) => {
  const url = "http://127.0.0.1:8080/api/profile/"
  const url1 = "http://127.0.0.1:8080/api/profile/edit"

  const [user,setUsers] = useState({
    name:"",
    username:"",
    email:"",
    password:"",
  })

  useEffect(()=>{
    axios.get(url,{headers: {
        "Authorization" : sessionStorage.getItem('token')
      }
    })
    .then(res=>{
      console.log(res.data);
      setUsers(res.data)
    }).catch(err=>console.error(err))

  },[])


  const handleSubmit = e => {
    e.preventDefault()
    axios.put(url1,user,{headers: {
			"Authorization" : sessionStorage.getItem('token')
		  }
		})
    .then(res=>{
      window.location.replace('/profile')
    }).catch(err=>console.error(err))
  }

  const handleChange = e => {
    const {name,value} = e.target
    setUsers({
      ...user,
        [name]:value
    })
  }

  return (
    <div className="container mt-5">
      <div className="card">
        <div className="card-header">
          Profile
        </div>
        <div className="card-body">
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label>Name</label>
            <input type="text" name="name" id="name" className="form-control" value={user.name} onChange={(e)=>handleChange(e)} />
          </div>
          <div className="form-group">
            <label>username</label>
            <input disabled type="text" name="username" id="username" className="form-control" value={user.username} onChange={(e)=>handleChange(e)} />
          </div>
          <div className="form-group">
            <label>Email</label>
            <input disabled type="email" name="email" id="email" className="form-control" value={user.email} onChange={(e)=>handleChange(e)} />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input type="password" name="password" id="name" className="form-control"  onChange={(e)=>handleChange(e)} />
          </div>
          <button className="btn btn-primary">Submit</button>
        </form>
        </div>
      </div>
    </div>
  )


}

export default UserProfile
