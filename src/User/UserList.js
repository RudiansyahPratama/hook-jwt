import React,{ useState,useEffect }from 'react';
import axios from 'axios'
import { Container } from 'reactstrap';


const UsersList = (props) => {
    const url = "http://127.0.0.1:8080/api/users"
    const [user,setUser] = useState({user:[]})
    const [role,setRole] = useState({role : ""})

    useEffect(() => {
        axios.get(url,{headers: {
			"Authorization" : sessionStorage.getItem('token')
		  }
        })
        .then(res=>{
            console.table(res.data)
            setUser(res.data)
        }).catch(err=>console.error(err))
    },[])

    const handleChange = e => {
      const {value} = e.target
      setRole({
        role:value
      })
    }

    const handleEdit = (userId) => {
      const data = {
        roleId:role.role
      }
      axios.put(`http://127.0.0.1:8080/api/users/${userId}`,data,{headers: {
      "Authorization" : sessionStorage.getItem('token')
      }
    })
    .then(res=>{
      props.history.push('/user')
    }).catch(err=>console.error(err))

    }

    const display = user.user.map(users=>
         <tr key={users.id}>
            <td>{users.name}</td>
            <td>{users.username}</td>
            <td>{users.email}</td>
            {/* {users.roles.map((role,index)=>
            <td key={index}>
                {role.name}
                </td>*/}
                <td><select name="role" onChange={handleChange}>
                  <option value={users.roles[0].id}>
                      {users.roles[0].name}
                  </option>
                  <option value="1">
                  User
                  </option>
                  <option value="2">
                    Admin
                  </option>
                  </select></td>
                  <td><button onClick={()=>{handleEdit(users.id)}} className="btn btn-success">Edit</button></td>

         </tr>

    )
    console.log(role)


    return (
        <div className="App">
            <div className="container mt-5">
                <div className="table-responsive">
            <table className="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role</th>
                    </tr>
                </thead>
                <tbody>
                    {display}
                </tbody>
            </table>
            </div>
            </div>
        </div>
    )
}

export default UsersList
