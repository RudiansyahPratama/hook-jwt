import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";

const Navs = props => {
  const [isOpen, setIsOpen] = useState(false);

  let Token = sessionStorage.getItem("token")
  let Role = sessionStorage.getItem("roles")
  const Logout = () => {
    Token = sessionStorage.clear();
    window.location.replace("/login")

  }
  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="dark" dark expand="md">
        <NavbarBrand href="/">reactstrap</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          {Token ?
            <>
            <Nav className="mr-auto" navbar>
            {Role === "2" ? (
              <>
                <NavItem>
                  <NavLink href="/book/">Book</NavLink>
               </NavItem>
               <NavItem >
                  <NavLink href="/user/">User</NavLink>
              </NavItem>
               </>
            ):(
              <>

            </>
            )}

            </Nav>
            <Nav className="ml-auto">
            <NavItem>
              <NavLink href="/profile">Profile</NavLink>
            </NavItem>
            <NavItem>
               <NavLink onClick={Logout} href="#">Logout</NavLink>
             </NavItem>
             </Nav>
             </>
          :
          <>
          <Nav className="ml-auto" navbar>
          <NavItem>
            <NavLink href="/login/">Login</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="/register/">Register</NavLink>
          </NavItem>
          </Nav>
          </>
          }
        </Collapse>
      </Navbar>
    </div>
  );
};

export default Navs;
