import React,{useEffect,useState} from 'react';
import axios from 'axios'
import logo from './logo.svg';
import './App.css';


function App(props) {
const url = "http://127.0.0.1:8080/api/books/"
const  [book,setBook] = useState({book:[]})


useEffect(() => {
  const fetchData = async () => {
    const result = await axios(url,{headers: {
    "Authorization" : sessionStorage.getItem('token')
    }
  });
  console.table(result.data.book)
    setBook(result.data);
  };
  fetchData();
  }, []);

  const Detail = (id) => {
    window.location.replace('/detail/' + id)
  }

  return (
    <div className="App mt-5 container">
    <div className="row row-cols-2 row-cols-md-2 container">
    {book.book.map(books=>
      <>
      <div key={books.id} className="col mb-4" >
        <div className="card">
          <div className="card-body">
            <h2 className="card-title">Judul Buku : {books.title}</h2>
            <h5 className="card-title">Author : {books.author}</h5>
            <button onClick={()=>Detail(books.id)} className="btn btn-outline-info">Detail</button>
          </div>
        </div>
      </div>

      </>
    )}
    </div>
    </div>
  );
}

export default App;
